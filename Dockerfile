ARG BUILD_DATE
ARG GIT_COMMIT_ID
ARG VERSION
ARG DOCKER_PHP_VERSION=8.1
ARG SCM_URL

FROM php:$DOCKER_PHP_VERSION-alpine

VOLUME /data

ADD ./resources /resources

ARG VERSION
ENV VERSION ${VERSION}

RUN echo ${VERSION}  && if [ -d /resources/${VERSION} ]; then /resources/${VERSION}/build; else /resources/build; fi && rm -rf /resources

WORKDIR /data

LABEL "maintainer"="guidon" \
      "org.label-schema.name"="moustache-ci" \
      "org.label-schema.base-image.name"="docker.io/php/$DOCKER_PHP_VERSION-alpine" \
      "org.label-schema.base-image.version"="$DOCKER_PHP_VERSION-alpine" \
      "org.label-schema.cmd"="docker run guidon/moustache-ci:$VERSION php" \
      "org.label-schema.description"="moustache CI/CD containers." \
      "org.label-schema.url"="https://gitlab.com/gui-don/moustache-ci-docker" \
      "org.label-schema.vcs-url"="https://gitlab.com/gui-don/moustache-ci-docker" \
      "org.label-schema.vcs-ref"=$GIT_COMMIT_ID \
      "org.label-schema.vendor"="guidon" \
      "org.label-schema.schema-version"="1.0.0-rc.1" \
      "org.label-schema.applications.php.version"=$DOCKER_PHP_VERSION \
      "org.label-schema.version"=$VERSION \
      "org.label-schema.build-date"=$BUILD_DATE \
      "org.label-schema.usage"="${SCM_URL}/src?at=${VERSION}"
